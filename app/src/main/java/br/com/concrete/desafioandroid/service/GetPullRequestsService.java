package br.com.concrete.desafioandroid.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.desafioandroid.model.PullRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class GetPullRequestsService extends IntentService {

    private final static String TAG = GetPullRequestsService.class.getSimpleName();
    private ArrayList<PullRequest> pullRequests;

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public GetPullRequestsService() {
        super("GetPullRequestsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            final ResultReceiver receiver = intent.getParcelableExtra("prReceiver");
            final String owner = intent.getStringExtra("owner");
            final String repo = intent.getStringExtra("repo");

            final Bundle bundle = new Bundle();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GitHubService.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            GitHubService service = retrofit.create(GitHubService.class);

            final Call<List<PullRequest>> request = service.getPullRequests(owner, repo);

            request.enqueue(new Callback<List<PullRequest>>() {
                @Override
                public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                    if (!response.isSuccessful()) {
                        Log.i(TAG, "Erro: " + response.code());
                    } else {
                        // Success
                        pullRequests = new ArrayList<>(response.body());
                        for (PullRequest pr: pullRequests) {
                            Log.i(TAG, "Pull Request: " + pr.getTitle());
                            Log.i(TAG, "===============================");
                        }
                        bundle.putParcelableArrayList("pullRequests", pullRequests);
                        receiver.send(STATUS_FINISHED, bundle);
                    }
                }

                @Override
                public void onFailure(Call<List<PullRequest>> call, Throwable t) {

                }
            });

        }
    }
}
