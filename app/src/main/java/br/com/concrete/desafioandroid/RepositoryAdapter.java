package br.com.concrete.desafioandroid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.concrete.desafioandroid.model.Repository;

/**
 * Created by Rodrigo Rocha on 12/10/2016.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepoViewHolder> {

    private List<Repository> repositoryList;
    private Context mContext;

    public class RepoViewHolder extends RecyclerView.ViewHolder {
        public TextView repoName, description, forksCount, starsCount, username, nameOfUser;
        public ImageView userAvatar;

        public RepoViewHolder(View view) {
            super(view);
            repoName = (TextView) view.findViewById(R.id.repo_name_textview);
            description = (TextView) view.findViewById(R.id.description_textview);
            forksCount = (TextView) view.findViewById(R.id.forks_count_textview);
            starsCount = (TextView) view.findViewById(R.id.stars_count_textview);
            username = (TextView) view.findViewById(R.id.username_textview);
            nameOfUser = (TextView) view.findViewById(R.id.name_of_user_textview);
            userAvatar = (ImageView) view.findViewById(R.id.user_avatar_imageview);
        }
    }

    public RepositoryAdapter(List<Repository> repositoryList, Context context) {
        this.repositoryList = repositoryList;
        this.mContext = context;
    }

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_list_row, parent, false);

        return new RepoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RepoViewHolder holder, int position) {
        Repository repository = repositoryList.get(position);
        holder.repoName.setText(repository.getName());
        holder.description.setText(repository.getDescription());
        holder.forksCount.setText(String.valueOf(repository.getForks()));
        holder.starsCount.setText(String.valueOf(repository.getStars()));
        holder.username.setText(repository.getUser().getLogin());
        holder.nameOfUser.setText(repository.getUser().getLogin());
        Glide.with(mContext)
                .load(repository.getUser().getAvatarUrl())
                .crossFade()
                .into(holder.userAvatar);
    }

    @Override
    public int getItemCount() {
        return repositoryList.size();
    }
}
