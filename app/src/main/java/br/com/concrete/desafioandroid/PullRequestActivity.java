package br.com.concrete.desafioandroid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import br.com.concrete.desafioandroid.model.PullRequest;
import br.com.concrete.desafioandroid.model.Repository;
import br.com.concrete.desafioandroid.service.GetPopularRepoService;
import br.com.concrete.desafioandroid.service.GetPullRequestsService;

public class PullRequestActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<PullRequest> pullRequestList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PullRequestAdapter mAdapter;
    private PRResultReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.pull_request_recycler_view);

        Intent intent = new Intent(Intent.ACTION_SYNC, null, this, GetPullRequestsService.class);
        mReceiver = new PRResultReceiver(new Handler());
        String owner = getIntent().getStringExtra("owner");
        String repo = getIntent().getStringExtra("repo");
        intent.putExtra("owner", owner);
        intent.putExtra("repo", repo);
        intent.putExtra("prReceiver", mReceiver);
        startService(intent);
    }

    private class PRResultReceiver extends ResultReceiver {

        public PRResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            pullRequestList = resultData.getParcelableArrayList("pullRequests");
            mAdapter = new PullRequestAdapter(pullRequestList, PullRequestActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PullRequestActivity.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(PullRequestActivity.this, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(PullRequestActivity.this, recyclerView,
                    new RecyclerTouchListener.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            String pullRequestUrl = pullRequestList.get(position).getPullRequestUrl();
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(pullRequestUrl));
                            startActivity(intent);
                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
        }
    }

}
