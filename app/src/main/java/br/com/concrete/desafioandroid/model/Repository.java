package br.com.concrete.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rodrigo Rocha on 11/10/2016.
 */

public class Repository {

    private String name;

    @SerializedName("owner")
    private User user;
    private String description;
    private int forks;

    @SerializedName("stargazers_count")
    private int stars;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}
