package br.com.concrete.desafioandroid;

import android.content.Intent;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.concrete.desafioandroid.model.PopularRepos;
import br.com.concrete.desafioandroid.model.Repository;
import br.com.concrete.desafioandroid.service.GetPopularRepoService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<Repository> repositoryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RepositoryAdapter mAdapter;
    private RepoResultReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.repository_recycler_view);

        Intent intent = new Intent(Intent.ACTION_SYNC, null, this, GetPopularRepoService.class);
        mReceiver = new RepoResultReceiver(new Handler());
        intent.putExtra("repoReceiver", mReceiver);
        startService(intent);
    }

    private class RepoResultReceiver extends ResultReceiver {

        public RepoResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            PopularRepos catalog = resultData.getParcelable("catalog");
            repositoryList = catalog.getRepositories();
            mAdapter = new RepositoryAdapter(repositoryList, MainActivity.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(MainActivity.this, recyclerView,
                    new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    String owner = repositoryList.get(position).getUser().getLogin();
                    String repo = repositoryList.get(position).getName();
                    Intent intent = new Intent(MainActivity.this, PullRequestActivity.class);
                    intent.putExtra("owner", owner);
                    intent.putExtra("repo", repo);
                    startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
        }
    }
}
