package br.com.concrete.desafioandroid.service;

import java.util.List;

import br.com.concrete.desafioandroid.model.PopularRepos;
import br.com.concrete.desafioandroid.model.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Rodrigo Rocha on 11/10/2016.
 */

public interface GitHubService {

    String API_BASE_URL = "https://api.github.com/";
    String GITHUB_BASE_URL = "https://github.com";

    @GET("/search/{search}")
    Call<PopularRepos> listPopRepos(@Path("search") String search,
                                    @Query("q") String query,
                                    @Query("sort") String sort,
                                    @Query("page") int page);

    @GET("/repos/{owner}/{repo}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("owner") String owner, @Path("repo") String repo);


}
