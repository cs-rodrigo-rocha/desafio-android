package br.com.concrete.desafioandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rodrigo Rocha on 11/10/2016.
 */

public class User implements Parcelable {

    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public User() {}
    private User(Parcel in) {
        this.login = in.readString();
        this.avatarUrl = in.readString();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeString(login);
        destination.writeString(avatarUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //EVERYTHING NEEDS TO BE WRITTEN/LOADED IN THE SAME ORDER

    //this variable must be named CREATOR (all capital)
    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
