package br.com.concrete.desafioandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rodrigo Rocha on 13/10/2016.
 */

public class PullRequest implements Parcelable {

    private String title;
    private User user;

    @SerializedName("created_at")
    private String date;

    @SerializedName("html_url")
    private String pullRequestUrl;

    private String body;

    public PullRequest() {}
    public PullRequest(String title, User user, String date, String pullRequestUrl, String body) {
        this.title = title;
        this.user = user;
        this.date = date;
        this.pullRequestUrl = pullRequestUrl;
        this.body = body;
    }

    private PullRequest(Parcel in) {
        this.title = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.date = in.readString();
        this.pullRequestUrl = in.readString();
        this.body = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPullRequestUrl() {
        return pullRequestUrl;
    }

    public void setPullRequestUrl(String pullRequestUrl) {
        this.pullRequestUrl = pullRequestUrl;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeString(title);
        destination.writeParcelable(user, 0);
        destination.writeString(date);
        destination.writeString(pullRequestUrl);
        destination.writeString(body);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //EVERYTHING NEEDS TO BE WRITTEN/LOADED IN THE SAME ORDER

    //this variable must be named CREATOR (all capital)
    public static final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel source) {
            return new PullRequest(source);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };
}
