package br.com.concrete.desafioandroid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.concrete.desafioandroid.model.PullRequest;

/**
 * Created by Rodrigo Rocha on 12/10/2016.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PRViewHolder> {

    private List<PullRequest> pullRequestList;
    private Context mContext;

    public class PRViewHolder extends RecyclerView.ViewHolder {
        public TextView title, body, username, nameOfUser;
        public ImageView userAvatar;

        public PRViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title_textview);
            body = (TextView) view.findViewById(R.id.body_textview);
            username = (TextView) view.findViewById(R.id.username_textview);
            nameOfUser = (TextView) view.findViewById(R.id.name_of_user_textview);
            userAvatar = (ImageView) view.findViewById(R.id.user_avatar_imageview);
        }
    }

    public PullRequestAdapter(List<PullRequest> pullRequestList, Context context) {
        this.pullRequestList = pullRequestList;
        this.mContext = context;
    }

    @Override
    public PRViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pull_request_list_row, parent, false);

        return new PRViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PRViewHolder holder, int position) {
        PullRequest pullRequest = pullRequestList.get(position);
        holder.title.setText(pullRequest.getTitle());
        holder.body.setText(pullRequest.getBody());
        holder.username.setText(pullRequest.getUser().getLogin());
        holder.nameOfUser.setText(pullRequest.getUser().getLogin());
        Glide.with(mContext)
                .load(pullRequest.getUser().getAvatarUrl())
                .crossFade()
                .into(holder.userAvatar);
    }

    @Override
    public int getItemCount() {
        return pullRequestList.size();
    }
}
