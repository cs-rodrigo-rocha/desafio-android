package br.com.concrete.desafioandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodrigo Rocha on 11/10/2016.
 */

public class PopularRepos implements Parcelable {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("items")
    private ArrayList<Repository> repositories;

    public PopularRepos() {}
    private PopularRepos(Parcel in) {
        this.totalCount = in.readInt();
        this.repositories = in.readArrayList(Repository.class.getClassLoader());
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(ArrayList<Repository> repositories) {
        this.repositories = repositories;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeInt(totalCount);
        destination.writeList(repositories);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //EVERYTHING NEEDS TO BE WRITTEN/LOADED IN THE SAME ORDER

    //this variable must be named CREATOR (all capital)
    public static final Creator<PopularRepos> CREATOR = new Creator<PopularRepos>() {
        @Override
        public PopularRepos createFromParcel(Parcel source) {
            return new PopularRepos(source);
        }

        @Override
        public PopularRepos[] newArray(int size) {
            return new PopularRepos[size];
        }
    };
}
