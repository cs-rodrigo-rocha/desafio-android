package br.com.concrete.desafioandroid.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.desafioandroid.model.PopularRepos;
import br.com.concrete.desafioandroid.model.Repository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class GetPopularRepoService extends IntentService {

    private final static String TAG = GetPopularRepoService.class.getSimpleName();
    private List<Repository> repositories = new ArrayList<>();
    private PopularRepos catalog;

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public GetPopularRepoService() {
        super("GetPopularRepoService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            final ResultReceiver receiver = intent.getParcelableExtra("repoReceiver");
            final Bundle bundle = new Bundle();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GitHubService.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            GitHubService service = retrofit.create(GitHubService.class);

            final Call<PopularRepos> repoCatalog = service.listPopRepos("repositories", "language:Java", "stars", 1);

            repoCatalog.enqueue(new Callback<PopularRepos>() {
                @Override
                public void onResponse(Call<PopularRepos> call, Response<PopularRepos> response) {
                    if (!response.isSuccessful()) {
                        Log.i(TAG, "Erro: " + response.code());
                    } else {
                        // Success
                        catalog = response.body();
                        bundle.putParcelable("catalog", catalog);
                        receiver.send(STATUS_FINISHED, bundle);

                        Log.i(TAG, "Quantidade de Repositórios: " + repositories.size());
                    }
                }

                @Override
                public void onFailure(Call<PopularRepos> call, Throwable t) {
                    Log.e(TAG, "Erro: " + t.getMessage());
                }
            });
        }
    }

}
